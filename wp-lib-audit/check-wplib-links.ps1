﻿param (
    $LinksCsv
)

function test-url{
    param(
        $id,
        $url,
        $rurl
    )

    $is_good = $true
    $notes = @()

    try {
        $u = new-object System.Uri -ArgumentList $url
        $p = [System.Web.HttpUtility]::ParseQueryString($u.Query)

        #check for redirects
        if ($url -ne $rurl) {
            $is_good = $false
            $notes += "~~~~> Redirected"
            $u = New-Object System.Uri -ArgumentList $rurl
        }
        #starts with https
        if (-not($u.Scheme -eq "https")) {
            $is_good = $false
            $notes += "Scheme is not HTTPS"
        }
        #host is www-cmswire.simplermedia.com
        if (-not($u.Host -eq "www-cmswire.simplermedia.com")) {
            $is_good = $false
            $notes += "Host is not 'www-cmswire.simplermedia.com'"
        }
        $qp = [System.Web.HttpUtility]::ParseQueryString($u.Query)
        #check tags
        if ($qp.Count -ne 4) {
            $is_good = $false
            $notes += "Expect 4 parameters. Got '$($qp.Count)'"
        }
        #tags
        #smg_source=cmswire.com
        if ($qp.Get('smg_source') -ne 'cmswire.com') {
            $is_good = $false
            $notes += "smg_source is not set to 'cmswire.com'"
        }
        #smg_medium=web
        if ($qp.Get('smg_medium') -ne 'web') {
            $is_good = $false
            $notes += "smg_medium is not set to 'web'"
        }
        #smg_content=whitepapers
        if ($qp.Get('smg_content') -ne 'whitepapers') {
            $is_good = $false
            $notes += "smg_content is not set to 'whitepapers'"
        }
        #smg_campaign=<same as page>
        if ($qp.Get('smg_campaign') -ne [System.IO.Path]::GetFileNameWithoutExtension($u.LocalPath)) {
            $is_good = $false
            $notes += "smg_campaign does not match landing page name"
        }
    } catch {
        $is_good = $false
        $notes += "Exception processing"
        $notes += $Error[0]        
    }

    return ( new-object psobject -Property @{ is_good=$is_good; notes=$notes} )
}

Add-Type -AssemblyName System.Web

if (-not(Test-Path $LinksCsv -PathType Leaf -ErrorAction Stop)) {
    throw "Failed to locate input file: '$LinksCsv'"
}

$data = import-csv -Path $LinksCsv

foreach ($entry in $data ) {
    $res = test-url -id $entry.id -url $entry.url -rurl $entry.rurl
    if ($res.is_good -ne $true) {
        Write-Warning $entry.id
        Write-Host ("===>{0}" -f $entry.url ) -BackgroundColor Black
        Write-Host ("~~~>{0}" -f $entry.rurl) -BackgroundColor Black
        Write-Host $res.notes
    }
}
Write-Host "DONE"