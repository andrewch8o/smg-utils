function scrape-wpsection-links{
    param (
        $sectionUrl
    )

    $out = @()
    Write-Progress -Activity "Processing $sectionUrl"
    #Load page
    $r = Invoke-WebRequest -Uri $sectionUrl
    #scan for all 'a' elements
    $al = $r.ParsedHtml.getElementsByClassName('whitepaper-title')
    #get title, link
    $al | % {
        $title = $_ | select -ExpandProperty textContent
        $url = $_ | select -ExpandProperty href
        #extract ID from the link ( reuse )
        $u = new-object System.Uri -ArgumentList $url
        $id = [System.IO.Path]::GetFileNameWithoutExtension($u.LocalPath)
        #check for redirects
        $rr = Invoke-WebRequest -MaximumRedirection 0 -ErrorAction SilentlyContinue -Uri $url
        $rurl = $url
        if ($rr.StatusCode -ne 200) {
            $rurl = $rr.Headers.Location
        }

        $e = New-Object psobject -Property @{ id=$id; title=$title; url=$url; rurl=$rurl }
        $out += $e
    }
    return $out
}

$SCRIPT:OUT = @()

# loading page
$r = invoke-webrequest "https://www.cmswire.com/white-papers/"                                                                                  
# loading & filtering divs by class
$ds = $r.ParsedHtml.getElementsByTagName('div')                                                                                                 
$dds = $ds | ? {$_.className -match 'whitepaper-category-title'}                                                                                
# reading 'a' elements from the div
$al = $dds | % {
    $_.getElementsByTagName('a') | select -expandProperty pathname
} | sort | Get-Unique

$al | % {
    $res = scrape-wpsection-links "https://www.cmswire.com/$_`?cb=$(Get-Date -UFormat %s)" # add cache buster token
    $SCRIPT:OUT += $res
    "$_, $($res.Count)" | Write-Host
}

$outPath = "wpscan-$(Get-Date -Format 'MM-dd-yy').csv"
$SCRIPT:OUT | select id,title,url,rurl | export-csv -NoClobber -NoTypeInformation -Path $outPath -Force

"Report @$outPath" | write-host