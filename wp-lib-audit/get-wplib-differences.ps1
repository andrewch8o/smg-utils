﻿<#
USAGE
.\get-wplib-differences.ps1 -LibAssetsFile ..\wplib-assets.txt -ActiveAssetsFile ..\cmpgn-assets.txt
#>

param (
    $LibAssetsFile,
    $ActiveAssetsFile
)

$libAssets    = Get-Content $LibAssetsFile -ErrorAction Stop
$activeAssets = Get-Content $ActiveAssetsFile -ErrorAction Stop

#Normalizing entries & filtering out empty lines
$libAssets    = $libAssets    | ForEach-Object { $_.Trim().ToLower() } | Where-Object { -not([String]::IsNullOrEmpty($_)) }
$activeAssets = $activeAssets | ForEach-Object { $_.Trim().ToLower() } | Where-Object { -not([String]::IsNullOrEmpty($_)) }

#Comparing
$wponlyList = @()
foreach ($libEntry in $libAssets) {
    if (-not($activeAssets -contains $libEntry)) {
        $wponlyList += $libEntry
    }
}
Write-Host ("Only in WP LIBRARY ( but not in ACTIVE CAMPAIGNS ): {0}" -f $wponlyList.Length)
$wponlyList | %{ Write-Host $_ }

$assetOnlyList = @()
foreach ($cmpEntry in $activeAssets) {
    if (-not($libAssets -contains $cmpEntry)) {
        $assetOnlyList += $cmpEntry
    }
}
Write-Host ("Only in ACTIVE CAMPAIGNS ( but not in WP LIBRARY ): {0}" -f $assetOnlyList.Length)
$assetOnlyList | %{ Write-Host $_ }

