"""The script tests redirects that have been configured for landing pages
Consumes text file as input parameter with each asset id on new line
Performs requests to each asset landing page using two known hostnames
Confirms return code is 200 & the page redirected to the whitepapers library 
"""

import os
import csv
import time
import logging
import argparse
import requests
import urllib.parse
from tqdm import tqdm
from dataclasses import dataclass

class URLs:
    wp_lib = 'https://www.cmswire.com/white-papers/'
    wwwcms = 'https://www-cmswire.simplermedia.com/'
    www2smg = 'https://www2.simplermedia.com/'

@dataclass
class TestResult:
    resp: int = 0
    lp_url: str = None
    resp_url: str = None

def test_domain(asset, on_domain):
    """Tests redirects for the asset on particular domain URL"""
    result = TestResult()
    asset_url = urllib.parse.urljoin(on_domain, asset + '.html')
    try:
        resp = requests.get(asset_url)
        result.resp = resp.status_code
        result.lp_url = asset_url
        result.resp_url = resp.url
    except Exception as e:
        logging.warning(f"Failed to process '{asset_url}' due to {str(e)}")
    return result

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s')
    parser = argparse.ArgumentParser(description="Test redirects for input assets")
    parser.add_argument('-i', dest='ifile', required=True, metavar='INPUT_FILE',
                        help='Input text file. Asset ids to process, each on new line')
    args = parser.parse_args()
    ifile = os.path.abspath(args.ifile)
    results = []
    data = []
    with open(ifile) as input:
        data = input.readlines()

    for line in tqdm(data, desc="Assets processed", unit=""):
        asset_id = line.strip()
        check1 = test_domain(asset=asset_id, on_domain=URLs.www2smg)
        results.append(check1)
        time.sleep(300/1000)
        check2 = test_domain(asset=asset_id, on_domain=URLs.wwwcms)
        results.append(check2)
        time.sleep(300/1000)
    
    with open('redirects_status.csv', mode='w+', newline='') as out:
        writer = csv.writer(out, dialect='excel', )
        for i in results:
            writer.writerow([i.resp, i.lp_url, i.resp_url])
