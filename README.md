Repository for "ad-hoc" utilities that were used to automate repeated manual operations.

Some of the utilities were used frequently enough to evolve:

* Web utilities to automate production of HTML ADs from the content of landing pages (these were proven to speed up AD production at least 5x).
    * token-dfp-ads.html
    * multi-item-cpl-ads.html
    * banners-cpl-ads.html
    * x-promos/x-promo.html

* Whitepaper library audit (ensuring that deactivated landing pages are not being promoted in whitepapers library). These were proven to save at least 1hr every week and resulted in better quality of the task.
    * wp-lib-audit/scan-wplib-links.ps1 
    * wp-lib-audit/get-wplib-differences.ps1
    * wp-lib-audit/check-wplib-links.ps1